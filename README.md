# README #

### What is this repository for? ###

* Unity games
* Unity 4.3

### What does it include? ###

* Unity project already set up for git
* Basic folder structure set up
* Navigation window
* Logger settings
* Simple Time wrapper

### How do I get set up? ###

* Fork this repository (check the privacy settings!)
* Adjust this README for your project
* Get going!

### Need help? Got ideas? Contact me!###

* Facebook Joel van de Vorstenbosch
* LinkedIn JoelVDV
* Email narmdo@gmail.com