﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/*
 * Want a new category?
 * 
 * Simply add more 'topic's to the Topic enum
 */

/// <summary>
/// Logging topics
/// </summary>
public enum LogTopic
{
	Debug,
	GameLogic,
	Framework,
	Editor,

	Count //Used to simply get the number of topics
}

public static class Logger
{
	static string topicColor = "darkblue";


	/// <summary>
	/// Gets and sets whether warnings are being filtered
	/// </summary>
	public static bool FilterWarnings { get { return filterWarnings; } set { filterWarnings = value; } }

	static Dictionary<LogTopic, bool> topicState = new Dictionary<LogTopic, bool>();

	static bool filterWarnings;

	/// <summary>
	/// Logs a message under the given topic
	/// </summary>
	/// <param name="topic">Topic of the message</param>
	/// <param name="message">Message to log</param>
	/// <param name="warning">Whether the message is a warning</param>
	public static void Log(this MonoBehaviour behaviour, LogTopic topic, string message, bool warning = false)
	{
		Log(topic, message, warning, behaviour);
	}

	/// <summary>
	/// Logs a message under the given topic
	/// </summary>
	/// <param name="topic">Topic of the message</param>
	/// <param name="message">Message to log</param>
	/// <param name="warning">Whether the message is a warning</param>
	/// <param name="context">Context of the message</param>
	public static void Log(LogTopic topic, string message, bool warning = false, UnityEngine.Object context = null)
	{
		if ((!topicState.ContainsKey(topic) || topicState[topic]) || //topic is enabled or
			(warning && !FilterWarnings))							 //is a warning, and warning aren't filtered
		{
			if (warning)
				Debug.LogWarning("<b><color=" + topicColor + ">[" + Enum.GetName(typeof(LogTopic), topic) + "]</color></b> " + message, context);
			else
				Debug.Log("<b><color=" + topicColor + ">[" + Enum.GetName(typeof(LogTopic), topic) + "]</color></b> " + message, context);
		}
	}

	/// <summary>
	/// Sets whether a topic is displayed in the editor
	/// </summary>
	/// <param name="topic">Topic to set state of</param>
	/// <param name="enabled">New topic enabled state</param>
	public static void SetTopicState(LogTopic topic, bool enabled)
	{
		topicState[topic] = enabled;
	}

	/// <summary>
	/// Gets the display state of a topic
	/// </summary>
	/// <param name="topic">Topic to get state of</param>
	public static bool GetTopicState(LogTopic topic)
	{
		if (!topicState.ContainsKey(topic))
			return true;
		
		return topicState[topic];
	}
}
