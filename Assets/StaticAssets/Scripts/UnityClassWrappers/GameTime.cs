﻿using UnityEngine;
using System.Collections;

public class GameTime : MonoBehaviour
{
	/// <summary>
	/// Gets or sets the game speed
	/// </summary>
	public static float Speed { get { return speed; } set { speed = value; } }

	/// <summary>
	/// Gets the delta time modified by the game speed
	/// </summary>
	public static float DeltaTime { get { return paused ? 0f : Time.deltaTime * speed; } }

	/// <summary>
	/// Gets the game time since the level loaded (takes game speed and pausing into account)
	/// The scene must include a GameTime component for this to work
	/// </summary>
	public static float TimeSinceLevelLoad { get { return timeSinceLevelLoad; } }

	/// <summary>
	/// Gets or sets whether the game is paused
	/// </summary>
	public static bool Paused
	{
		get { return paused; }

		set
		{
			paused = value;
			Logger.Log(LogTopic.Framework, paused ? "Game paused." : "Game unpaused");
		}
	}

	static float speed = 1f;
	static bool paused = false;

	static float timeSinceLevelLoad;




	//Component related

	/// <summary>
	/// Control for the current scene
	/// </summary>
	static GameTime control;

	void Start()
	{
		if (control != null)
			Debug.LogError("A TimeControl component already exists in this scene.");

		control = this;
		timeSinceLevelLoad = 0f;
	}

	void Update()
	{
		timeSinceLevelLoad += GameTime.DeltaTime;
	}
}
