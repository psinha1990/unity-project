﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtensions
{
	public static Interface GetInterfaceComponent<Interface>(this GameObject gameObject) where Interface : class
	{
		return gameObject.GetComponent(typeof(Interface)) as Interface;
	}
}
