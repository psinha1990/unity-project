﻿using UnityEngine;
using System.Collections;

public static class TransformExtensions
{
	//Setting

	/// <summary>
	/// Sets the X coordinate of the transform
	/// </summary>
	/// <param name="x">X coordinate</param>
	public static void SetWorldX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x = x;
		transform.position = position;
	}

	/// <summary>
	/// Sets the Y coordinate of the transform
	/// </summary>
	/// <param name="y">Y coordinate</param>
	public static void SetWorldY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y = y;
		transform.position = position;
	}

	/// <summary>
	/// Sets the Z coordinate of the transform
	/// </summary>
	/// <param name="z">Z coordinate</param>
	public static void SetWorldZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z = z;
		transform.position = position;
	}


	/// <summary>
	/// Sets the local X coordinate of the transform
	/// </summary>
	/// <param name="x">Local X coordinate</param>
	public static void SetLocalX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x = x;
		transform.position = position;
	}

	/// <summary>
	/// Sets the local Y coordinate of the transform
	/// </summary>
	/// <param name="y">Local Y coordinate</param>
	public static void SetLocalY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y = y;
		transform.position = position;
	}

	/// <summary>
	/// Sets the local Z coordinate of the transform
	/// </summary>
	/// <param name="z">Local Z coordinate</param>
	public static void SetLocalZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z = z;
		transform.position = position;
	}


	//Add

	/// <summary>
	/// Adds to the X coordinate of the transform
	/// </summary>
	/// <param name="x">X coordinate addition</param>
	public static void AddWorldX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x += x;
		transform.position = position;
	}

	/// <summary>
	/// Adds to the Y coordinate of the transform
	/// </summary>
	/// <param name="y">Y coordinate addition</param>
	public static void AddWorldY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y += y;
		transform.position = position;
	}

	/// <summary>
	/// Adds to the Z coordinate of the transform
	/// </summary>
	/// <param name="z">Z coordinate addition</param>
	public static void AddWorldZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z += z;
		transform.position = position;
	}


	/// <summary>
	/// Adds to the local X coordinate of the transform
	/// </summary>
	/// <param name="x">Local X coordinate addition</param>
	public static void AddLocalX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x += x;
		transform.position = position;
	}

	/// <summary>
	/// Adds to the local Y coordinate of the transform
	/// </summary>
	/// <param name="y">Local Y coordinate addition</param>
	public static void AddLocalY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y += y;
		transform.position = position;
	}

	/// <summary>
	/// Adds to the local Z coordinate of the transform
	/// </summary>
	/// <param name="z">Local Z coordinate addition</param>
	public static void AddLocalZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z += z;
		transform.position = position;
	}


	//Multiplying

	/// <summary>
	/// Multiplies the X coordinate of the transform
	/// </summary>
	/// <param name="x">X coordinate multiplier</param>
	public static void MultiplyWorldX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x *= x;
		transform.position = position;
	}

	/// <summary>
	/// Multiplies the Y coordinate of the transform
	/// </summary>
	/// <param name="y">Y coordinate multiplier</param>
	public static void MultiplyWorldY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y *= y;
		transform.position = position;
	}

	/// <summary>
	/// Multiplies the Z coordinate of the transform
	/// </summary>
	/// <param name="z">Z coordinate multiplier</param>
	public static void MultiplyWorldZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z *= z;
		transform.position = position;
	}


	/// <summary>
	/// Multiplies the local X coordinate of the transform
	/// </summary>
	/// <param name="x">Local X coordinate multiplier</param>
	public static void MultiplyLocalX(this Transform transform, float x)
	{
		Vector3 position = transform.localPosition;
		position.x *= x;
		transform.position = position;
	}

	/// <summary>
	/// Multiplies the local Y coordinate of the transform
	/// </summary>
	/// <param name="y">Local Y coordinate multiplier</param>
	public static void MultiplyLocalY(this Transform transform, float y)
	{
		Vector3 position = transform.localPosition;
		position.y *= y;
		transform.position = position;
	}

	/// <summary>
	/// Multiplies the local Z coordinate of the transform
	/// </summary>
	/// <param name="z">Local Z coordinate multiplier</param>
	public static void MultiplyLocalZ(this Transform transform, float z)
	{
		Vector3 position = transform.localPosition;
		position.z *= z;
		transform.position = position;
	}
}
