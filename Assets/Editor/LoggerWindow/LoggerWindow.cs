﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using Narmdo.Internal;
using System;

[System.Serializable]
public class LoggerWindow : EditorWindow
{
	Vector2 scrollPosition;


	[MenuItem("Window/Narmdo/Logger")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		LoggerWindow window = EditorWindow.GetWindow(typeof(LoggerWindow), false, "Logger Settings") as LoggerWindow;

		window.Show();
	}

	
	void OnGUI()
	{
		Logger.FilterWarnings = EditorGUILayout.ToggleLeft("  Filter Warnings", Logger.FilterWarnings);

		EditorGUILayout.Space();

		scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

		for (int i = 0; i < (int)LogTopic.Count; i++)
		{
			LogTopic topic = (LogTopic)i;

			string topicName = Enum.GetName(typeof(LogTopic), topic);

			bool newState = EditorGUILayout.ToggleLeft("  " + topicName, Logger.GetTopicState(topic));

			Logger.SetTopicState(topic, newState);
		}

		EditorGUILayout.EndScrollView();

		EditorGUILayout.LabelField("To modify the logger topics, go to StaticAssets/ Scripts/ UnityClassWrappers/ Logger.cs", EditorStyles.wordWrappedLabel);
	}
}
