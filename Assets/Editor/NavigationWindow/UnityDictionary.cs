﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Narmdo.Internal
{
	/// <summary>
	/// Dodgy dictionary for Serializing. Not for anything big!
	/// </summary>
	[Serializable]
	public class UnityDictionary<Key, Value>
	{
		public int Count
		{
			get { return keys.Count; }
		}

		public KeyValuePair<Key, Value> this[int index]
		{
			get
			{
				return new KeyValuePair<Key, Value>(keys[index], values[index]);
			}
		}

		public Value this[Key key]
		{
			get
			{
				int index = keys.IndexOf(key);

				return values[index];
			}

			set
			{
				int index = keys.IndexOf(key);

				if (index == -1)
				{
					keys.Add(key);
					values.Add(value);
				}
				else
				{
					values[index] = value;
				}
			}
		}

		[SerializeField]
		private List<Key> keys = new List<Key>();

		[SerializeField]
		private List<Value> values = new List<Value>();

		public void Add(Key key, Value value)
		{
			this[key] = value;
		}

		public void Remove(Key key)
		{
			int index = keys.IndexOf(key);

			if (index != -1)
			{
				keys.RemoveAt(index);
				values.RemoveAt(index);
			}
		}

		public bool ContainsKey(Key key)
		{
			return keys.Contains(key);
		}
	}
}