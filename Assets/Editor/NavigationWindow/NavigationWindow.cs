﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using Narmdo.Internal;

[System.Serializable]
public class NavigationWindow : EditorWindow
{
	[System.Serializable]
	class FolderDictionary : UnityDictionary<string, NavigationFolder> { }

	[System.Serializable]
	class SceneDictionary : UnityDictionary<string, string> { }

	[System.Serializable]
	class NavigationFolder
	{
		public List<string> contents;

		public FolderDictionary folders;

		public bool open;
		
		public NavigationFolder(bool open = false)
		{
			this.open = open;
		}
	}


	[SerializeField]
	NavigationFolder scenes;

	[SerializeField]
	Vector2 scrollPos;
	[SerializeField]
	Vector2 scrollPos2;

	Texture2D lockIcon;
	Texture2D lockOpenIcon;

	[SerializeField]
	List<string> locked = new List<string>();

	[SerializeField]
	bool autoUpdate;

	[SerializeField]
	List<string> allScenesFullPath;

	[SerializeField]
	SceneDictionary shortToFullPath;

	List<string> searchResults;

	string search = "";

	[MenuItem("Window/Narmdo/Navigation")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		NavigationWindow window = EditorWindow.GetWindow(typeof(NavigationWindow), false, "Project Navigation") as NavigationWindow;

		window.UpdateSceneList();
		window.Show();
	}

	void OnEnable()
	{
		hideFlags = HideFlags.HideAndDontSave;

		string[] assetDir = Directory.GetDirectories(Application.dataPath, "NavigationWindow.*", SearchOption.AllDirectories);
		
		string icon1 = assetDir[0] + "/Lock.png";
		icon1 = icon1.Substring(Application.dataPath.Length + 1);

		string icon2 = assetDir[0] + "/Lock_open.png";
		icon2 = icon2.Substring(Application.dataPath.Length + 1);

		lockIcon = AssetDatabase.LoadAssetAtPath("Assets/" + icon1, typeof(Texture2D)) as Texture2D;
		lockOpenIcon = AssetDatabase.LoadAssetAtPath("Assets/" + icon2, typeof(Texture2D)) as Texture2D;
	}

	void OnInspectorUpdate()
	{
		for (int i = 0; i < locked.Count; i++)
		{
			bool fail = true;
			string fileName = locked[i];

			foreach (string scene in allScenesFullPath)
			{
				if (scene.EndsWith(fileName))
				{
					fail = false;
					break;
				}
			}

			if (fail)
			{
				locked.RemoveAt(i);
				i--;
			}
		}

		Repaint();
	}

	void SwitchTo(string scene)
	{
		if (EditorApplication.isPlaying)
		{
			Logger.Log(LogTopic.Editor, "Please stop running the game before changing scene.", true);
			return;
		}
		
		if (EditorApplication.SaveCurrentSceneIfUserWantsTo())
		{
			Logger.Log(LogTopic.Editor, "Switching to scene: " + scene);
			EditorApplication.OpenScene("Assets/" + shortToFullPath[scene] + ".unity");
			EditorApplication.isPaused = false;
		}
	}

	void OnGUI()
	{
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		
		DisplayFolder(scenes);

		EditorGUILayout.EndScrollView();

		EditorGUI.indentLevel = 0;

		GUILayout.Space(10f);

		EditorGUILayout.LabelField("Locked");

		for (int i = 0; i < locked.Count; i++)
		{
			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(20f);

			if (GUILayout.Button(locked[i]))
			{
				SwitchTo(locked[i]);
			}

			if (GUILayout.Button(lockIcon, GUILayout.Width(30f)))
			{
				locked.RemoveAt(i);
				i--;
			}

			EditorGUILayout.EndHorizontal();
		}

		GUILayout.Space(10f);

		EditorGUILayout.LabelField("Search");

		EditorGUILayout.BeginHorizontal();

		string oldSearch = search;
		search = EditorGUILayout.TextField(search);

		if (GUILayout.Button("Clear", GUILayout.Width(80f)))
		{
			search = "";
			Debug.Log(allScenesFullPath.Count);
		}
		EditorGUILayout.EndHorizontal();

		if (oldSearch != search && search != "")
			searchResults = allScenesFullPath.FindAll(x => x.Contains(search));

		if (search != "")
		{
			scrollPos2 = EditorGUILayout.BeginScrollView(scrollPos2);

			for (int i = 0; i < searchResults.Count; i++)
			{
				string label = searchResults[i];
				if (label.Contains("\\"))
					label = label.Substring(searchResults[i].LastIndexOf('\\') + 1);

				DisplaySceneButton(label, 1);
			}

			EditorGUILayout.EndScrollView();
		}

		GUILayout.Space(10f);

		GUILayout.FlexibleSpace();

		EditorGUILayout.BeginHorizontal();

		if (GUILayout.Button("Update"))
			UpdateSceneList();

		//autoUpdate = EditorGUILayout.ToggleLeft("Auto Update", autoUpdate, GUILayout.Width(90f));

		EditorGUILayout.EndHorizontal();
	}

	void DisplaySceneButton(string scene, int indent = 0)
	{
		EditorGUILayout.BeginHorizontal();

		GUILayout.Space(20f * indent);

		if (GUILayout.Button(scene))
		{
			SwitchTo(scene);
		}

		if (locked.Contains(scene) && GUILayout.Button(lockIcon, GUILayout.Width(30f)))
		{
			locked.Remove(scene);
		}

		if (!locked.Contains(scene) && GUILayout.Button(lockOpenIcon, GUILayout.Width(30f)))
		{
			locked.Add(scene);
			locked.Sort();
		}

		EditorGUILayout.EndHorizontal();
	}

	void DisplayFolder(NavigationFolder folder, int indent = 0)
	{
		foreach (string scene in folder.contents)
		{
			DisplaySceneButton(scene, indent);
		}

		EditorGUI.indentLevel = indent;

		for (int i = 0; i < folder.folders.Count; i++)
		{
			KeyValuePair<string, NavigationFolder> folderPair = folder.folders[i];

			folderPair.Value.open = EditorGUILayout.Foldout(folderPair.Value.open, folderPair.Key);

			if (folderPair.Value.open)
			{
				DisplayFolder(folderPair.Value, indent + 1);
			}
		}

		EditorGUI.indentLevel = indent - 1;
	}

	void UpdateSceneList()
	{
		scenes = new NavigationFolder();
		scenes.contents = new List<string>();
		scenes.folders = new FolderDictionary();
		shortToFullPath = new SceneDictionary();

		string[] files = Directory.GetFiles(Application.dataPath, "*.unity", SearchOption.AllDirectories);

		allScenesFullPath.Clear();

		foreach (string file in files)
		{
			AddScene(file);
		}
    }

	void AddScene(string fileName)
	{
		int start = Application.dataPath.Length + 1;
		fileName = fileName.Substring(start, fileName.Length - start - 6);

		allScenesFullPath.Add(fileName);

		string[] parts = fileName.Split('\\');

		NavigationFolder currentFolder = scenes;

		for (int i = 0; i < parts.Length - 1; i++)
		{
			if (!currentFolder.folders.ContainsKey(parts[i]))
			{
				NavigationFolder folder = new NavigationFolder(i == 0);
				folder.contents = new List<string>();
				folder.folders = new FolderDictionary();

				currentFolder.folders.Add(parts[i], folder);
			}

			currentFolder = currentFolder.folders[parts[i]];
		}

		currentFolder.contents.Add(parts[parts.Length - 1]);
		shortToFullPath.Add(parts[parts.Length - 1], fileName);
	}
}
